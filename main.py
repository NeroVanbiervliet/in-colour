import colorsys
import tkinter
import cv2
import numpy as np
from synthesizer import Player, Synthesizer, Waveform

UPDATE_LOOP_MS = 10
AMPLITUDE_GAIN = 3

video_capture = None
player = None
tk_canvas = None
tk_root = None
main_colour = "#000000"

def get_camera_colour():
    ret, frame = video_capture.read()
    avg_per_row = np.average(frame, axis=0)
    blue, green, red = np.average(avg_per_row, axis=0)
    hex_colour = '#%02x%02x%02x' % (int(red), int(green), int(blue))
    hls_colour = colorsys.rgb_to_hls(red/255, green/255, blue/255)
    return hex_colour, hls_colour

def hls_to_frequency(hls):
    h, l, s = hls  # h = 0.0 -> 1.0
    low_c = 261
    high_c = 523

    return h * (high_c-low_c) + low_c

def hls_to_amplitude(hls):
    _, _, saturation = hls

    # no sound if it is too gray
    if saturation < 0.1:
        return 0

    amplitude = saturation * AMPLITUDE_GAIN
    capped = min(amplitude, 1)
    return capped

def update():
    hex, hls = get_camera_colour()
    frequency = hls_to_frequency(hls)
    amplitude = hls_to_amplitude(hls)

    # update audio & canvas
    synth = Synthesizer(osc1_waveform=Waveform.sine, osc1_volume=amplitude, use_osc2=False)
    player.play_wave(synth.generate_constant_wave(frequency, 0.05))
    tk_canvas.config(bg=hex)

    # reschedule update
    tk_root.after(UPDATE_LOOP_MS, update)

def main():
    global video_capture, player, synth, tk_root, tk_canvas

    video_capture = cv2.VideoCapture(0)
    player = Player()
    player.open_stream()

    tk_root = tkinter.Tk()
    tk_canvas = tkinter.Canvas(tk_root, width=200, height=200, bg=main_colour)
    tk_canvas.pack()
    tk_root.after(UPDATE_LOOP_MS, update)
    tk_root.mainloop()

    video_capture.release()


if __name__ == "__main__":
    main()