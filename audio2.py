from synthesizer import Player, Synthesizer, Waveform

player = Player()
player.open_stream()
synthesizer = Synthesizer(osc1_waveform=Waveform.sawtooth, osc1_volume=1.0, use_osc2=False)

frequencies = [440]*10 + [500]*5 + [440]*10 + [520]*5 + [440]*10 + [500]*5 + [440]*10 + [520]*5

for f in frequencies:
    player.play_wave(synthesizer.generate_constant_wave(f, 0.1))